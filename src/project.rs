use std::borrow::Cow;
use std::path::PathBuf;

use serde::{Deserialize, Serialize};
use tabled::Tabled;

#[derive(Serialize, Deserialize, Debug, Tabled)]
pub struct Project {
    pub name: String,
    #[tabled(display_with = "format_path")]
    pub path: PathBuf,
}

fn format_path(path: &PathBuf) -> Cow<'_, str> {
    path.to_string_lossy()
}
