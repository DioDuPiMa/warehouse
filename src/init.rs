use std::error::Error;
use std::fs;
use std::fs::File;
use std::path::Path;
use std::process::Command;

use crate::InitArgs;
use crate::project::Project;
use crate::warehouse::Warehouse;

fn check_is_dir_is_repo(path: &Path) -> bool {
    let output = Command::new("git")
        .arg("rev-parse")
        .arg("--is-inside-work-tree")
        .current_dir(path)
        .output()
        .expect("Failed to execute command");

    output.status.success()
}

fn get_repo_remotes(path: &Path) {
    let output = Command::new("git")
        .arg("remote")
        .arg("-v")
        .current_dir(path)
        .output()
        .expect("Failed to execute command");

    let remote_table = String::from_utf8_lossy(&output.stdout);
    println!("{}", remote_table)
}

pub fn init_command(args: &InitArgs) -> Result<(), Box<dyn Error>> {
    let warehouse = Warehouse { folder_path: args.projects_path.to_path_buf() };
    fs::create_dir_all(warehouse.config_folder_path())?;
    let output = File::create(&warehouse.config_file_path())?;
    let paths = fs::read_dir(&args.projects_path)?;
    let mut projects: Vec<Project> = Vec::new();

    for path in paths {
        let project_path = path.unwrap().path();
        let project_config = Project { name: project_path.file_name().unwrap().to_str().unwrap().to_string(), path: project_path };
        let valid = check_is_dir_is_repo(project_config.path.as_path());
        get_repo_remotes(project_config.path.as_path());
        if valid {
            println!("Adding project: {}", project_config.name);
            projects.push(project_config);
        }
    }

    serde_json::to_writer_pretty(output, &projects).unwrap();
    Ok(())
}
