use std::error::Error;

use tabled::Table;

use crate::ShowArgs;
use crate::warehouse::Warehouse;

pub fn show_command(args: &ShowArgs) -> Result<(), Box<dyn Error>> {
    let warehouse = Warehouse { folder_path: args.projects_path.to_path_buf() };
    let config = warehouse.read_config();
    let table = Table::new(config).to_string();
    println!("{}", table);
    Ok(())
}