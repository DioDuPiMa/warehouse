use std::fs::File;
use std::io::BufReader;
use std::path::PathBuf;

use crate::project::Project;

pub struct Warehouse {
    pub(crate) folder_path: PathBuf,
}

impl Warehouse {
    pub fn config_folder_path(&self) -> PathBuf {
        self.folder_path.join(".warehouse")
    }

    pub fn config_file_path(&self) -> PathBuf {
        self.config_folder_path().join("projects.json")
    }

    pub fn read_config(&self) -> Vec<Project> {
        let file = File::open(&self.config_file_path()).unwrap();
        let reader = BufReader::new(file);
        let config: Vec<Project> = serde_json::from_reader(reader).unwrap();
        config
    }
}