use std::path::PathBuf;

use clap::{Args, Parser, Subcommand};

mod init;
mod show;
mod project;
mod warehouse;

#[derive(Parser)]
#[command(name = "Warehouse")]
#[command(author = "DioDuPiMA")]
#[command(version = "0.1")]
#[command(propagate_version = true)]
#[command(about = "Git Projects management", long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Init warehouse git projects folder.
    Init(InitArgs),
    Show(ShowArgs),
}

#[derive(Args)]
pub struct InitArgs {
    /// Init a warehouse
    projects_path: PathBuf,
}

#[derive(Args)]
pub struct ShowArgs {
    /// See current projects in the warehouse
    projects_path: PathBuf,
}

#[derive(Args)]
pub struct AddArgs {
    /// add remote project to the warehouse
    projects_path: PathBuf,
}

fn main() {
    let cli = Cli::parse();
    match &cli.command {
        Commands::Init(args) => init::init_command(args).unwrap(),
        Commands::Show(args) => show::show_command(args).unwrap()
    }
}
